<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmpresaRepository")
 */
class Empresa
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\Column(type="integer")
     */
    private $cnpj;

    /**
     * @ORM\ManyToMany(targetEntity="Socio", inversedBy="empresas")
     * @ORM\JoinTable(
     *     name="empresa_socio",
     *     joinColumns={
     *          @ORM\JoinColumn(name="empresa_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="socio_id", referencedColumnName="id")
     *     }
     * )
     */
    private $socios;

    /**
     * Empresa constructor.
     * @param $socios
     */
    public function __construct()
    {
        $this->socios = new ArrayCollection();
    }

    public function addSocios (Socio $socio) {
        $this->socios[] = $socio;

        return $this;
    }

    public function removeSocios (Socio $socio) {
        $this->socios->removeElement($socio);

    }

    /**
     * @return ArrayCollection
     */
    public function getSocios()
    {
        return $this->socios;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getCnpj(): ?int
    {
        return $this->cnpj;
    }

    public function setCnpj(int $cnpj): self
    {
        $this->cnpj = $cnpj;

        return $this;
    }

    public function __toString() {
        return $this->nome;
    }
}
