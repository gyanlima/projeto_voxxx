<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SocioRepository")
 */
class Socio
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $nome;

    /**
     * @ORM\ManyToMany(targetEntity="Empresa", mappedBy="socios")
     */
    private $empresas;

    public function __construct()
    {
        $this->empresas = new ArrayCollection();
    }

    public function addSocios (Socio $empresa) {
        $this->empresas[] = $empresa;

        return $this;
    }

    public function removeEmpresas (Socio $empresa) {
        $this->empresas->removeElement($empresa);

    }

    /**
     * @return ArrayCollection
     */
    public function getEmpresas()
    {
        return $this->empresas;
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function __toString() {
        return $this->nome;
    }
}
