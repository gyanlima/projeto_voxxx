<?php

namespace App\Repository;

use App\Entity\Socio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Socio|null find($id, $lockMode = null, $lockVersion = null)
 * @method Socio|null findOneBy(array $criteria, array $orderBy = null)
 * @method Socio[]    findAll()
 * @method Socio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SocioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Socio::class);
    }

    // /**
    //  * @return Socio[] Returns an array of Socio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Socio
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getEmpresaSocios($id) {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT socio.id as id_socio,
        (select nome from empresa where empresa.id = empresa_socio.empresa_id) as empresa,
        (select id from empresa where empresa.id = empresa_socio.empresa_id) as id_empresa
        FROM socio
        LEFT JOIN empresa_socio ON socio.id = empresa_socio.socio_id
        WHERE socio.id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("id", $id);
        $stmt->execute();

        // returns an array of arrays (i.e. a raw data set)
        return $stmt->fetchAll();


    }

    public function insertEmpresaSocio(array $empresa_id,$socio_id) {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'INSERT INTO empresa_socio(empresa_id, socio_id) VALUES (:empresa_id, :socio_id)';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("empresa_id", $empresa_id);
        $stmt->bindValue("socio_id", $socio_id);
        $stmt->execute();

    }
}
