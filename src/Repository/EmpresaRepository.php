<?php

namespace App\Repository;

use App\Entity\Empresa;
use App\Entity\Socio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Empresa|null find($id, $lockMode = null, $lockVersion = null)
 * @method Empresa|null findOneBy(array $criteria, array $orderBy = null)
 * @method Empresa[]    findAll()
 * @method Empresa[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmpresaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Empresa::class);
    }

    // /**
    //  * @return Empresa[] Returns an array of Empresa objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Empresa
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getSociosEmpresa($id) {

        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT empresa.id,
                (select nome from socio where socio.id = empresa_socio.socio_id) as socio,
				(select id from socio where socio.id = empresa_socio.socio_id) as id_socio
                FROM empresa
                LEFT JOIN empresa_socio ON empresa.id = empresa_socio.empresa_id
                WHERE empresa.id = :id';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue("id", $id);
        $stmt->execute();


        return $stmt->fetchAll();


    }
}
