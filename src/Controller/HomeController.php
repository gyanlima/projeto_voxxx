<?php

namespace App\Controller;

use App\Repository\EmpresaRepository;
use App\Repository\SocioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(EmpresaRepository $empresaRepository, SocioRepository $socioRepository): Response
    {
        return $this->render('home/index.html.twig', [
            'empresas' => $empresaRepository->findAll(),
            'socios' => $socioRepository->findAll()
        ]);
    }
}
