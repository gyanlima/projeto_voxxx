CRUD Empresa-Socios

*Não consegui gerar o projeto na versão 4.3, então utilizei a 4.4*

BANCO:
user: postgres
senha: root

1º php bin/console doctrine:database:create
2º php bin/console doctrine:schema:update --force
3º composer require symfony/web-server-bundle --dev


Para executar digite o comando php bin/console server:run

Todas as funções CRUD estão no Menu: "Ações"